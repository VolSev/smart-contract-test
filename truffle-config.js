const HDWalletProvider = require("truffle-hdwallet-provider");
const fs = require("fs");
module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
      provider: new HDWalletProvider(fs.readFileSync('/Users/volodymyr/Desktop/BlockChain/firelabs/smart-contract-test/local.env', 'utf-8'), "http://localhost:8545"),
    },
    "inf_firelabs-task_ropsten": {
      network_id: 3,
      gasPrice: 100000000000,
      provider: new HDWalletProvider(fs.readFileSync('/Users/volodymyr/Desktop/BlockChain/firelabs/smart-contract-test/test.env', 'utf-8'), "https://ropsten.infura.io/v3/0f5f1cfb0a644810b381d45fe1a4af13"),
      // from: "0xc154A7CA0E1d66397623e1B0cDed885776CfBE20"
    }
  },
  compilers: {
    solc: {
      version: "0.7.0"
    },
  }
};
