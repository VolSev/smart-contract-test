import React, { Component, Fragment } from "react";
import EventManager from "./contracts/EventManager.json";
import getWeb3 from "./getWeb3";

import "./App.css";

class App extends Component {
  state = { 
    events: [], 
    web3: null, 
    accounts: null,
    isOwner: null,
    contract: null, 
    newEvent: {
      name: null,
      description: null,
      ticketPrice: null,
      ticketsQuantity: null,
      startTimestamp: null,
      endTimestamp: null,
    },
    addressForVerification: null};

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      window.ethereum.on('accountsChanged', function () {
        window.location.reload();
      })
      

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = EventManager.networks[networkId];

      const instance = new web3.eth.Contract(
        EventManager.abi,
        deployedNetwork && deployedNetwork.address,
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      const isOwner = await instance.methods.isOwner(accounts[0]).call();
      this.setState({ web3, accounts, contract: instance, isOwner });
      this.listEvents();
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  reassembleObject = (arraysObject) => {
    const events = [];
    arraysObject.names.forEach((name, index) => {
      const event = {
        id: +arraysObject.ids[index],
        name,
        description: arraysObject.descriptions[index],
        ticketPrice: this.state.web3.utils.fromWei(arraysObject.ticketPrices[index]),
        ticketsQuantity: +arraysObject.ticketsQuantities[index],
        ticketsSymbol: arraysObject.ticketsSymbols[index],
        startTimestamp: +arraysObject.startTimestamps[index],
        endTimestamp: +arraysObject.endTimestamps[index]
      }
      events.push(event)
    })
    return events;
  }

  createEvent = async () => {
    const { accounts, contract, newEvent } = this.state;
    const event = {
      ...newEvent,
      ticketPrice: this.state.web3.utils.toWei(newEvent.ticketPrice),
      ticketsQuantity: +newEvent.ticketsQuantity,
      startTimestamp: +newEvent.startTimestamp,
      endTimestamp: +newEvent.endTimestamp
    }
    await contract.methods.createEvent(event.name, event.ticketsSymbol, event.description, event.ticketPrice, event.ticketsQuantity, event.startTimestamp, event.endTimestamp).send({ from: accounts[0] });

  }

  listEvents = async () => {
    const { contract, accounts } = this.state;
    const response = await contract.methods.listEvents().call();
    let events = this.reassembleObject(response);
    events = await Promise.all(events.map(async (event) => {
      event.ownedTicketsCount = +(await contract.methods.getBalanceOfTickets(event.id).call({from: accounts[0] }));
      return event;
    }));
    this.setState({ events });
  }

  buyTicket = async (eventId) => {
    const { contract, events, accounts } = this.state;
    const event = events.find(({id}) => id === eventId)
    const ethValue = this.state.web3.utils.toWei(event.ticketPrice.toString())
    await contract.methods.buyTicket(event.id).send({ from: accounts[0], value: ethValue });
  }

  verifyTicket = async (eventId, eventIndex) => {
    const { contract, addressForVerification, events } = this.state;
    
    const response = await contract.methods.doesAddressHasTicket(addressForVerification, eventId).call();
    let newEvents = events;
    newEvents[eventIndex] = {
      ...newEvents[eventIndex],
      ownerAddressVerified: response
    }
    this.setState({...this.state, events: newEvents})
    return response;
  }

  handleInputChange = async (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
        [name]: value
    });

  }

  handleNewEventInputChange = async (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    
    this.setState({
      "newEvent": {
        ...this.state["newEvent"],
        [name]: value
      }
    });

  }

  getOwnTicketCountHTML = (count) => {
    if (count === 0) {
      return;
    }
    return <Fragment>You already have {count} ticket(s) for this event.</Fragment>
  }

  render() {
    if (!this.state.web3) {
      return <div>Loading Web3, accounts, and contract...</div>;
    }
    const eventCreationHTML = this.state.isOwner ? (<Fragment><input onChange={this.handleNewEventInputChange} name="name" placeholder="Name"></input>
    <br></br>
    <input onChange={this.handleNewEventInputChange} name="description" placeholder="Description"></input>
    <br></br>
    <input onChange={this.handleNewEventInputChange} name="ticketPrice" placeholder="Ticket Price"></input>
    <br></br>
    <input onChange={this.handleNewEventInputChange} name="ticketsQuantity" placeholder="Tickets Quantity"></input>
    <br></br>
    <input onChange={this.handleNewEventInputChange} name="ticketsSymbol" placeholder="Tickets Symbol"></input>
    <br></br>
    <input onChange={this.handleNewEventInputChange} name="startTimestamp" placeholder="Start Timestamp"></input>
    <br></br>
    <input onChange={this.handleNewEventInputChange} name="endTimestamp" placeholder="End Timestamp"></input>
    <br></br>
    <button onClick={this.createEvent}>Create Event</button></Fragment>) : (<Fragment></Fragment>)
    const eventBlockStyle = {textAlign: "left", border: "solid", borderWidth: "1px", margin: "20px", padding: "10px"};
    const ticketValidationHTML = (event, eventIndex) => {
      let verificationText;
      if (event.ownerAddressVerified === true) {
        verificationText = "This wallet has ticket for this event."
      } else if (event.ownerAddressVerified === false) {
        verificationText = "This wallet has no tickets for this event."
      }
      return this.state.isOwner ? (<Fragment>
        <br></br>
        <br></br>
        <input onChange={this.handleInputChange} name="addressForVerification" placeholder="Address to verify"></input>
        <br></br>
        <br></br>
        <button onClick={() => this.verifyTicket(event.id, eventIndex)}>Verify Ticket</button>
        <br></br>
        {verificationText}
        </Fragment>) : (<Fragment></Fragment>)
    }             

    return (
      <div className="App">
        <br></br>
        <br></br>
        <br></br>
        {eventCreationHTML}
        <button onClick={this.listEvents}>List Events</button>
        <div>
          Events: {
            this.state.events.map((event, index) => 
            <div style={eventBlockStyle} key={index}>
              Name: {event.name}
              <br></br>
              Description: {event.description}
              <br></br>
              Ticket Price: {event.ticketPrice} ETH
              <br></br>
              Tickets Quantity: {event.ticketsQuantity}
              <br></br>
              Tickets Symbol: {event.ticketsSymbol}
              <br></br>
              Start Time: {new Date(event.startTimestamp).toDateString()}
              <br></br>
              End Time: {new Date(event.endTimestamp).toDateString()}
              <br></br>
              <br></br>
              <button onClick={() => this.buyTicket(event.id)}>Buy Ticket</button>
              {this.getOwnTicketCountHTML(event.ownedTicketsCount)}
              {ticketValidationHTML(event, index)}
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

export default App;
