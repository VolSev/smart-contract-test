// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.8.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./TicketManager.sol";

contract EventManager is Ownable {
    using Counters for Counters.Counter;

    Counters.Counter counter;

    constructor() Ownable() {}

    struct Event {
        string name;
        string description;
        string ticketSymbol;
        uint256 ticketPrice;
        uint256 ticketsQuantity;
        uint256 eventId;
        uint256 startTimestamp;
        uint256 endTimestamp;
    }

    Event[] events;
    mapping(uint256 => TicketManager) public ticketManagers;

    function createEvent(
        string memory name,
        string memory ticketSymbol,
        string memory description,
        uint256 ticketPrice,
        uint256 ticketsQuantity,
        uint256 startTimestamp,
        uint256 endTimestamp
    ) public onlyOwner returns (uint256) {
        uint256 eventId = Counters.current(counter);
        TicketManager ticketManager =
            new TicketManager(
                name,
                ticketSymbol,
                description,
                ticketPrice,
                ticketsQuantity,
                eventId,
                startTimestamp,
                endTimestamp
            );
        ticketManagers[eventId] = ticketManager;
        Event memory _event =
            Event(
                name,
                description,
                ticketSymbol,
                ticketPrice,
                ticketsQuantity,
                eventId,
                startTimestamp,
                endTimestamp
            );
        events.push(_event);
        Counters.increment(counter);
        return eventId;
    }

    function buyTicket(uint256 eventId) public payable {
        address mintTo = msg.sender;
        uint256 eventIndex = getEventIndex(eventId);
        Event memory _event = events[eventIndex];
        require(
            msg.value == _event.ticketPrice,
            "You've payed less than required to buy a ticket."
        );
        _event.ticketsQuantity = _event.ticketsQuantity - 1;
        events[eventIndex] = _event;
        TicketManager ticketManager = ticketManagers[eventId];
        ticketManager.mint(mintTo);
    }

    function doesAddressHasTicket(address owner, uint256 eventId)
        public
        view
        onlyOwner
        returns (bool)
    {
        TicketManager ticketManager = ticketManagers[eventId];
        uint256 addressBalance = ticketManager.balanceOf(owner);
        return addressBalance != 0;
    }

    function getBalanceOfTickets(uint256 eventId)
        public
        view
        returns (uint256)
    {
        TicketManager ticketManager = ticketManagers[eventId];
        return ticketManager.balanceOf(msg.sender);
    }

    function isOwner(address addr) public view returns (bool) {
        address owner = Ownable.owner();
        return addr == owner;
    }

    function getEvent(uint256 id)
        public
        view
        returns (
            uint256[] memory ids,
            string[] memory names,
            string[] memory descriptions,
            string[] memory ticketsSymbol,
            uint256[] memory ticketPrices,
            uint256[] memory ticketsQuantities,
            uint256[] memory startTimestamps,
            uint256[] memory endTimestamps
        )
    {
        uint256 index = getEventIndex(id);
        Event[] memory _tempArray = new Event[](1);
        _tempArray[0] = events[index];
        return (converEventArrayToValueArrays(_tempArray));
    }

    function getEventIndex(uint256 eventId)
        private
        view
        returns (uint256 index)
    {
        for (index = 0; index < events.length; index++) {
            if (events[index].eventId == eventId) {
                return index;
            }
        }
    }

    function listEvents()
        public
        view
        returns (
            uint256[] memory ids,
            string[] memory names,
            string[] memory descriptions,
            string[] memory ticketsSymbols,
            uint256[] memory ticketPrices,
            uint256[] memory ticketsQuantities,
            uint256[] memory startTimestamps,
            uint256[] memory endTimestamps
        )
    {
        return converEventArrayToValueArrays(events);
    }

    function converEventArrayToValueArrays(Event[] memory _events)
        private
        pure
        returns (
            uint256[] memory ids,
            string[] memory names,
            string[] memory descriptions,
            string[] memory ticketsSymbols,
            uint256[] memory ticketPrices,
            uint256[] memory ticketsQuantities,
            uint256[] memory startTimestamps,
            uint256[] memory endTimestamps
        )
    {
        ids = new uint256[](_events.length);
        names = new string[](_events.length);
        descriptions = new string[](_events.length);
        ticketsSymbols = new string[](_events.length);
        ticketPrices = new uint256[](_events.length);
        ticketsQuantities = new uint256[](_events.length);
        startTimestamps = new uint256[](_events.length);
        endTimestamps = new uint256[](_events.length);

        for (uint256 i = 0; i < _events.length; i++) {
            ids[i] = _events[i].eventId;
            names[i] = _events[i].name;
            descriptions[i] = _events[i].description;
            ticketsSymbols[i] = _events[i].ticketSymbol;
            ticketPrices[i] = _events[i].ticketPrice;
            ticketsQuantities[i] = _events[i].ticketsQuantity;
            startTimestamps[i] = _events[i].startTimestamp;
            endTimestamps[i] = _events[i].endTimestamp;
        }

        return (
            ids,
            names,
            descriptions,
            ticketsSymbols,
            ticketPrices,
            ticketsQuantities,
            startTimestamps,
            endTimestamps
        );
    }
}
