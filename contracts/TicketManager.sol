// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.8.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract TicketManager is ERC721, Ownable {
    using Counters for Counters.Counter;

    Counters.Counter counter;

    struct Ticket {
        uint256 id;
        uint256 eventId;
        address owner;
    }

    string Name;
    string description;
    uint256 ticketPrice;
    uint256 ticketsQuantity;
    uint256 eventId;
    uint256 startTimestamp;
    uint256 endTimestamp;

    Ticket[] tickets;

    constructor(
        string memory _name,
        string memory _ticketSymbol,
        string memory _description,
        uint256 _ticketPrice,
        uint256 _ticketsQuantity,
        uint256 _eventId,
        uint256 _startTimestamp,
        uint256 _endTimestamp
    ) ERC721(_name, _ticketSymbol) Ownable() {
        Name = _name;
        description = _description;
        ticketPrice = _ticketPrice;
        ticketsQuantity = _ticketsQuantity;
        eventId = _eventId;
        startTimestamp = _startTimestamp;
        endTimestamp = _endTimestamp;
    }

    modifier ticketsExist() {
        require(ticketsQuantity >= 1, "Tickets already sold out.");
        _;
    }

    function mint(address mintTo) public ticketsExist {
        uint256 _tokenId = Counters.current(counter);

        Ticket memory ticket = Ticket(_tokenId, eventId, mintTo);

        _safeMint(mintTo, _tokenId);

        tickets.push(ticket);

        ticketsQuantity = ticketsQuantity - 1;
        Counters.increment(counter);
    }
}
